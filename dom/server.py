# -*- encoding: utf-8 -*-
__author__ = 'Joanna667'
import os
import socket

def http_server(gniazdo):
    while True:
        connection, client_address = gniazdo.accept()
        try:
            try:
                request = connection.recv(1024)
                if request:
                    print ("Odebrano:")
                    print (request)
                    address = str(request.decode())
                    print (address)
                    if address[-4:] == "html":
                        try:
                            html = "HTTP/1.1 200 OK Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += open("web"+address, "r").read()
                        except:
                            html = "HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += "<h1>404 not found</h1>"
                    elif address[-3:] == "txt" or address[-2:] == "py" or address[-3:] == "rst":
                        try:
                            html = "HTTP/1.1 200 OK Content-Type: text/span; charset=UTF-8\r\n\r\n"
                            html += open("web"+address, "r").read()
                        except:
                            html = "HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += "<h1>404 not found</h1>"
                    elif address[-3:] == "png":
                        try:
                            html = "HTTP/1.0 200 OK Content-Type: image/png; charset=UTF-8\r\n\r\n"
                            html += '<img src="data:image/png;base64,{0}">'.format(open("web"+address, "rb").read())
                        except:
                            html = "HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += "<h1>404 not found</h1>"
                    elif address[-3:] == "jpg":
                        try:
                            html = "HTTP/1.1 200 OK Content-Type: image/jpeg; charset=UTF-8\r\n\r\n"
                            html += '<img src="data:image/jpeg;base64,{0}">'.format(open("web"+address, "rb").read())
                        except:
                            html = "HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += "<h1>404 not found</h1>"
                    elif not "." in address:
                        print (address)
                        try:
                            print (os.listdir("web"+address))
                            html = "HTTP/1.1 200 OK Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += "<!DOCTYPE html><html><head></head><body>"
                            items = os.listdir("web"+address)
                            for item in items:
                                print (address+"/"+item)
                                if "." not in item:
                                    item += "/"
                                html += "<a href=\""+item+"\">"+item+"</a><br />"
                            html += "</body></html>"
                        except:
                            html = "HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += "<h1>404 not found</h1>"
                    else:
                        try:
                            html = "HTTP/1.1 200 OK Content-Type: plain/text; charset=UTF-8\r\n\r\n"
                            html += open("web"+address, 'r').read()
                        except:
                            html = "HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
                            html += "<h1>404 not found</h1>"
            except:
                html = "HTTP/1.0 500 Internal Server Error Content-Type: text/html; charset=UTF-8\r\n\r\n"
                html += "<h1>500 Internal Server Error</h1>"
            connection.sendall(html.encode())
        finally:
            connection.close()


gniazdo = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
gniazdo.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
address = ('localhost', 5000)  # zmienić hosta na 194.29.175.240
gniazdo.bind(address)
gniazdo.listen(1)

try:
    http_server(gniazdo)

finally:
    gniazdo.close()
