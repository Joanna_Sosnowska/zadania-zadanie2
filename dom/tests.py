# -*- encoding: utf-8 -*-
__author__ = 'Joanna667'
import mimetypes
import socket
import unittest
import sys


CRLF = '\r\n'
KNOWN_TYPES = set(mimetypes.types_map.values())


class TestHeaders(unittest.TestCase):
    def send_message(self, message):
        """Próba wysłania żądania przy uzyciu testowego klienta.

        Informuje o problemie w przypadku błędu gniazda - jak w zadaniu z lab.
        """
        response = ''
        try:
            response = client(message)
        except socket.error, e:
            if e.errno == 61:
                msg = "Blad: {0}, czy serwer dziala?"
                self.fail(msg.format(e.strerror))
            else:
                self.fail("Niespodziewany blad: {0}".format(str(e)))
        return response

    def test_response_content_type_text(self):
        message = CRLF.join(['/lokomotywa.txt'])
        res = self.send_message(message)
        expected_name = 'Content-Type: text/span'
        self.assertTrue(expected_name in res)

    def test_response_content_type_jpg(self):
        message = CRLF.join(['/images/jpg_rip.jpg'])
        res = self.send_message(message)
        expected_name = 'Content-Type: image/jpeg'
        self.assertTrue(expected_name in res)

    def test_response_content_type_png(self):
        message = CRLF.join(['/images/gnu_meditate_levitate.png'])
        res = self.send_message(message)
        expected_name = 'Content-Type: image/png'
        self.assertTrue(expected_name in res)

    def test_response_content_type_html(self):
        message1 = CRLF.join(['/images'])
        message2 = CRLF.join(['/web_page.html'])
        messages=[message1, message2]
        for m in messages:
            res = self.send_message(m)
            expected_name = 'Content-Type: text/html'
            self.assertTrue(expected_name in res)

class HTTPServerAndTypesTest(unittest.TestCase):
    #test - czy serwer działa dla danych typów

    def send_message(self, message):
        """ta sama metoda testowa
        """
        response = ''
        try:
            response = client(message)
        except socket.error, e:
            if e.errno == 61:
                msg = "Blad: {0}, czy serwer dziala?"
                self.fail(msg.format(e.strerror))
            else:
                self.fail("Niespodziewany blad: {0}".format(str(e)))
        return response

    def test_folder(self):
        message = CRLF.join(['/images'])
        expected = '200 OK'
        actual = self.send_message(message)
        self.assertTrue(expected in actual)

    def test_txt(self):
        message = CRLF.join(['/lokomotywa.txt'])
        expected = '200 OK'
        actual = self.send_message(message)
        self.assertTrue(expected in actual)

    def test_html(self):
        message = CRLF.join(['/web_page.html'])
        expected = '200 OK'
        actual = self.send_message(message)
        self.assertTrue(expected in actual)

    def test_jpg(self):
        message = CRLF.join(['/images/jpg_rip.jpg'])
        expected = '200 OK'
        actual = self.send_message(message)
        self.assertTrue(expected in actual)

    def test_png(self):
        message = CRLF.join(['/images/gnu_meditate_levitate.png'])
        expected = '200 OK'
        actual = self.send_message(message)
        self.assertTrue(expected in actual)
    #plik nie znajdujący się w odpowiedniej lokalizacji
    def test_not_found(self):
        message = CRLF.join(['server.py'])
        expected = '404 Not Found'
        actual = self.send_message(message)
        self.assertTrue(expected in actual)

#testowe gniazdo i klient - TE SAME co w przykładzie z laboratorium - nie potrzeba innych
class SocketStub:

    def __init__(self, request):
        self.request = request
        self.response = ''
        self.offset = 0

    def recv(self, length):
        data = self.request[self.offset:self.offset+length]
        self.offset += length
        return data

    def send(self, response):
        self.response += response

    def sendall(self, response):
        self.response += response



def client(msg):
    server_address = ('localhost', 5000) #zmienić hosta na 194.29.175.240
    sock = socket.socket(
        socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP
    )
    print >>sys.stderr, u'podłączono do {0} na porcie {1}'.format(*server_address)
    sock.connect(server_address)
    response = ''
    done = False
    bufsize = 1024
    try:
        print >>sys.stderr, 'wysylanie "{0}"'.format(msg)
        sock.sendall(msg)
        while not done:
            chunk = sock.recv(bufsize)
            if len(chunk) < bufsize:
                done = True
            response += chunk
        print >>sys.stderr, 'otrzymano "{0}"'.format(response)
    finally:
        print >>sys.stderr, u'zamykanie połączenia'
        sock.close()
    return response


if __name__ == '__main__':
    unittest.main()
